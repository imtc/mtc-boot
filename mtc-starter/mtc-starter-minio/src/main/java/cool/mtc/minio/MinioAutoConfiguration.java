package cool.mtc.minio;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yz
 */
@Configuration
@ConditionalOnProperty(value = "mtc.minio.enabled", havingValue = "true")
@EnableConfigurationProperties({MinioProperties.class})
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MinioAutoConfiguration {
    private final MinioProperties minioProperties;

    @Bean
    public MinioTemplate minioTemplate() {
        return new MinioTemplate(minioProperties);
    }
}
