package cool.mtc.minio;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author yz
 */
@Data
@ConfigurationProperties(prefix = "mtc.minio")
public class MinioProperties {

    /**
     * 是否启用
     */
    private boolean enabled = false;

    /**
     * 对象存储服务的URL
     */
    private String endpoint;

    /**
     * 账户名
     */
    private String accessKey;

    /**
     * 账户密码
     */
    private String secretKey;
}
