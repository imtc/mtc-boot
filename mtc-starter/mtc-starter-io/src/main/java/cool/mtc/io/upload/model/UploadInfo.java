package cool.mtc.io.upload.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 明河
 * 上传信息
 */
@Data
public class UploadInfo {

    /**
     * 上传的类型
     */
    private String type;

    /**
     * 上传的文件
     */
    @JsonIgnore
    private MultipartFile file;

    /**
     * 是否完成
     */
    private boolean complete;

    /**
     * 新文件名
     */
    private String name;

    /**
     * 文件扩展名
     */
    private String extension;

    /**
     * 原文件名
     */
    private String originalName;

    /**
     * 文件类型
     */
    private String mime;

    /**
     * 文件的MD5值
     */
    private String md5;

    /**
     * 文件的SHA-256值
     */
    private String sha256;

    /**
     * 文件存放的相对路径
     */
    private String relativePath;

    /**
     * 文件存放的相对路径
     */
    private String absolutePath;

    /**
     * 是否以切片形式上传
     */
    private boolean chunk = false;

    /**
     * 切片文件存放的文件夹
     */
    private String chunkPath;

    /**
     * ID，唯一标识
     */
    private String chunkId;

    /**
     * 切片总数
     */
    private Integer chunkTotal;

    /**
     * 当前上传切片的顺序/编号
     */
    private Integer chunkIndex;
}
