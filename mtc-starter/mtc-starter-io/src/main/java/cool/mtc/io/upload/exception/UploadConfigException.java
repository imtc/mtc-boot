package cool.mtc.io.upload.exception;

import cool.mtc.core.exception.CustomException;
import cool.mtc.core.result.Result;

/**
 * @author 明河
 */
public class UploadConfigException extends CustomException {

    public UploadConfigException(Result<Object> result) {
        super(result);
    }
}
