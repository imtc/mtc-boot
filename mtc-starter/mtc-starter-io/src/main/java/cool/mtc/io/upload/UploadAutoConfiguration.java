package cool.mtc.io.upload;

import cool.mtc.io.upload.read.config.StaticResourceReadConfigurer;
import cool.mtc.io.upload.read.interceptor.PublicStaticResourceInterceptor;
import cool.mtc.io.upload.read.interceptor.StaticResourceInterceptorContext;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author 明河
 */
@Configuration
@Import({
        StaticResourceReadConfigurer.class,
        StaticResourceInterceptorContext.class
})
@EnableConfigurationProperties({UploadProperties.class})
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UploadAutoConfiguration {
    private final UploadProperties uploadProperties;

    @Bean
    public UploadTemplate uploadTemplate() {
        return new UploadTemplate(uploadProperties);
    }

    @Bean
    @ConditionalOnMissingBean(name = "publicStaticResourceInterceptor")
    public PublicStaticResourceInterceptor publicStaticResourceInterceptor() {
        return new PublicStaticResourceInterceptor();
    }
}
