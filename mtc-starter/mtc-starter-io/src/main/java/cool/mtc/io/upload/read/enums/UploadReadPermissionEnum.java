package cool.mtc.io.upload.read.enums;

import cool.mtc.core.enums.EnumSupport;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 明河
 * <p>
 * 读取文件的权限
 */
@Getter
@RequiredArgsConstructor
public enum UploadReadPermissionEnum implements EnumSupport {

    PUBLIC("无需登录即可读取", "publicStaticResourceInterceptor"),
    PROTECTED("登录可读取", "protectedStaticResourceInterceptor"),
    PRIVATE("与自己有关的可读取", "privateStaticResourceInterceptor"),
    CUSTOM("自定义读取权限", "customStaticResourceInterceptor"),
    ;

    private final String desc;
    private final String interceptorBeanName;
}
