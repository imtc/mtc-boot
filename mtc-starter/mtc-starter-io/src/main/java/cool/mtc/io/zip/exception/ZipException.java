package cool.mtc.io.zip.exception;

import cool.mtc.core.exception.CustomException;
import cool.mtc.core.result.Result;

/**
 * @author 明河
 */
public class ZipException extends CustomException {

    public ZipException(String message) {
        super(message);
    }

    public ZipException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ZipException(Result<Object> result) {
        super(result);
    }

    public ZipException(Result<Object> result, Throwable throwable) {
        super(result, throwable);
    }
}
