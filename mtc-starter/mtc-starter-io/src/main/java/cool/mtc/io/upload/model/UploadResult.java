package cool.mtc.io.upload.model;

import lombok.Data;

/**
 * @author 明河
 */
@Data
public class UploadResult {

    public UploadResult(int total) {
        this.total = total;
    }

    /**
     * 是否完成
     */
    private boolean complete = false;

    /**
     * 已完成上传的切片的数量
     */
    private int completeNum = 0;

    /**
     * 切片总数
     */
    private int total = 0;
}
