package cool.mtc.io.upload.exception;

import cool.mtc.core.exception.CustomException;
import cool.mtc.core.result.Result;

/**
 * @author 明河
 */
public class UploadParamException extends CustomException {

    public UploadParamException(String message) {
        super(message);
    }

    public UploadParamException(Result<Object> result) {
        super(result);
    }
}
