package cool.mtc.io.zip;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yz
 */
@Configuration
public class ZipAutoConfiguration {

    @Bean
    public ZipTemplate zipTemplate() {
        return new ZipTemplate();
    }
}
