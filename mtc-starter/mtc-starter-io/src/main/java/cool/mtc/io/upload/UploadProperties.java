package cool.mtc.io.upload;

import cool.mtc.core.util.StringUtil;
import cool.mtc.io.upload.read.enums.UploadReadPermissionEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.unit.DataSize;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author 明河
 */
@Data
@ConfigurationProperties(prefix = "mtc.io.upload")
public class UploadProperties {

    private boolean enabled = true;

    /**
     * 保存上传文件的基础路径
     */
    private String basePath = "classpath:/upload/";

    /**
     * 上传的类型
     */
    private List<Type> types = Collections.singletonList(Type.DEFAULT);

    /**
     * 切片配置
     */
    private Chunk chunk;

    @Data
    public static class Type {
        public static final Type DEFAULT = new Type();

        static {
            DEFAULT.setPath(StringUtil.EMPTY);
            DEFAULT.setAllowExtensions(Collections.singletonList("*"));
            DEFAULT.setChunk(Chunk.DEFAULT);
        }

        /**
         * 类型
         */
        private String type;

        /**
         * 是否依赖基础路径
         */
        private boolean dependBasePath = true;

        /**
         * 路径
         * 当依赖基础路径时，应为相对路径，否则为绝对路径
         */
        private String path;

        /**
         * 是否在保存文件时包一层uuid文件夹，可防止同名文件覆盖，默认使用
         */
        private boolean useUuidFolder = true;

        /**
         * 允许上传文件的最大大小，默认10MB
         */
        private DataSize maxSize = DataSize.ofMegabytes(10);

        /**
         * 允许的扩展名（注意：区分大小写），例：pdf/jpg等，为*时，表示不限
         */
        private List<String> allowExtensions = new ArrayList<>();

        /**
         * 切片配置
         */
        private Chunk chunk;

        /**
         * 读配置
         */
        private Read read = Read.DEFAULT;

        /**
         * 获取完整路径
         */
        public String getInusePath(UploadProperties properties) {
            return (this.isDependBasePath() ? properties.getBasePath() : StringUtil.EMPTY) + this.getPath();
        }

        /**
         * 获取切片配置
         */
        public Chunk getInuseChunk(UploadProperties properties) {
            return null == this.chunk ? properties.getChunk() : this.chunk;
        }
    }

    @Data
    public static class Chunk {
        public static final Chunk DEFAULT = new Chunk();

        /**
         * 是否启用切片上传
         */
        private boolean enabled = true;

        /**
         * 是否依赖基础路径
         */
        private boolean dependBasePath = true;

        /**
         * 切片文件存放路径
         */
        private String path = ".chunk/";

        /**
         * 切片大小：5M
         */
        private DataSize size = DataSize.ofMegabytes(3);

        /**
         * 是否在完成后删除切片文件
         */
        private boolean deleteAfterComplete = true;

        /**
         * 获取完整路径
         */
        public String getInusePath(UploadProperties properties) {
            return (this.isDependBasePath() ? properties.getBasePath() : StringUtil.EMPTY) + this.getPath();
        }
    }

    @Data
    public static class Read {
        private static final Read DEFAULT = new Read();

        static {
            DEFAULT.setPermission(UploadReadPermissionEnum.PUBLIC);
            DEFAULT.setRequestPathPatterns(Collections.singletonList("/static/resource/**"));
        }

        /**
         * 读权限
         */
        private UploadReadPermissionEnum permission;

        /**
         * 请求的路径
         */
        private List<String> requestPathPatterns = new ArrayList<>();
    }
}
