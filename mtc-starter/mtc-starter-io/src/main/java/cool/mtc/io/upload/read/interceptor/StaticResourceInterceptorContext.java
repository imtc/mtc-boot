package cool.mtc.io.upload.read.interceptor;

import cool.mtc.io.upload.read.enums.UploadReadPermissionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 明河
 */
@Component
public class StaticResourceInterceptorContext {
    private static final Map<String, StaticResourceInterceptor> INTERCEPTOR_MAP = new ConcurrentHashMap<>();

    @Autowired
    private void setInterceptorMap(Map<String, StaticResourceInterceptor> map) {
        INTERCEPTOR_MAP.clear();
        INTERCEPTOR_MAP.putAll(map);
    }

    public StaticResourceInterceptor getInterceptor(UploadReadPermissionEnum readPermission) {
        return INTERCEPTOR_MAP.get(readPermission.getInterceptorBeanName());
    }
}
