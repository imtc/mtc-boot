package cool.mtc.mybatis.plus.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author 明河
 */
@Data
public class EntityDO {

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 创建者
     */
    private Long creator;
}
