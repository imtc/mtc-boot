package cool.mtc.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 明河
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseDO extends EntityDO {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
}
