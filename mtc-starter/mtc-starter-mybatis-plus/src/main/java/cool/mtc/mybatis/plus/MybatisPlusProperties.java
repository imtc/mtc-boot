package cool.mtc.mybatis.plus;

import com.baomidou.mybatisplus.annotation.DbType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 明河
 */
@Data
@ConfigurationProperties(prefix = "mtc.mybatis.plus")
public class MybatisPlusProperties {

    /**
     * 数据库类型
     * 用于MybatisPlus分页
     */
    private DbType dbType = DbType.MYSQL;

    private String[] mapperScanBasePackages = {};
}
