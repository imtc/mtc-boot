package cool.mtc.redis.config;

import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Configuration;

/**
 * @author 明河
 */
@Configuration
public class RedisConfigurer extends CachingConfigurerSupport {
}
