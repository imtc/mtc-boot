package cool.mtc.web;

import lombok.Builder;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpMethod;

import java.util.Collections;
import java.util.List;

/**
 * @author 明河
 */
@Data
@ConfigurationProperties(prefix = "mtc.web")
public class WebProperties {

    /**
     * web页面的路径
     */
    private String path = "/web/";

    /**
     * 跨域配置
     */
    private List<Cors> cors = Collections.singletonList(Cors.DEFAULT);

    /**
     * 统一封装返回结果的基础包路径
     */
    private String[] globalResultWrapperBasePackages = {};

    /**
     * 统一异常拦截未定义的异常信息是否直接返回给客户端
     * 还是将异常详细信息对客户端隐藏，只返回错误
     * true->未定义异常信息暴露给客户端
     * false->未定义异常信息不暴露给客户端
     */
    private Boolean showExceptionDetail = false;

    /**
     * 分页的配置
     */
    private Page page = new Page();

    @Data
    @Builder
    public static class Cors {
        public static final Cors DEFAULT = Cors.builder()
                .enabled(true)
                .pathPattern("/**")
                .allowedOriginPatterns(new String[]{"*"})
                .allowedMethods(new String[]{HttpMethod.GET.name(), HttpMethod.POST.name(), HttpMethod.OPTIONS.name()})
                .allowedHeaders(new String[]{"Authorization", "Content-Type"})
                .exposedHeaders(new String[]{"Authorization"})
                .build();

        /**
         * 是否允许跨域
         */
        private boolean enabled;

        /**
         * 允许跨域的路径
         */
        private String pathPattern;

        /**
         * 允许跨域的源（准确匹配），不可以使用“*”
         */
        private String[] allowedOrigins;

        /**
         * 允许跨域的源（模式匹配），可以使用“*”
         */
        private String[] allowedOriginPatterns;

        /**
         * 允许跨域的请求方法
         */
        private String[] allowedMethods;

        /**
         * 允许的请求头
         */
        private String[] allowedHeaders;

        /**
         * 允许前端获取的响应头
         */
        private String[] exposedHeaders;
    }

    @Data
    public static class Page {

        /**
         * 默认页码
         */
        private int defaultPage = 1;

        /**
         * 默认每页数量
         */
        private int defaultPageSize = 10;

        /**
         * 请求中页码的参数名
         */
        private String pageParam = "page";

        /**
         * 请求中每页数量的参数名
         */
        private String pageSizeParam = "pageSize";
    }
}
