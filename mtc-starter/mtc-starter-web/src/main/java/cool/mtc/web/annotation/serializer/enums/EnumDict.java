package cool.mtc.web.annotation.serializer.enums;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cool.mtc.core.util.StringUtil;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author yz
 * <p>
 * 枚举字典数据序列化注解
 * <p>
 * 用于数据序列化时将翻译枚举值
 */
@JacksonAnnotationsInside
@JsonSerialize(using = EnumDictSerializer.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumDict {

    /**
     * 翻译字典值的属性名称
     */
    String displayNameFieldName() default StringUtil.EMPTY;

    String styleColorFileName() default StringUtil.EMPTY;
}
