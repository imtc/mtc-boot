package cool.mtc.web.model.form;

/**
 * @author 明河
 */
public interface FormSupport<T> {

    T getId();
}
