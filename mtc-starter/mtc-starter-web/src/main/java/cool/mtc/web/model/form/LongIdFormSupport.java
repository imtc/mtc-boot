package cool.mtc.web.model.form;

/**
 * @author 明河
 */
public interface LongIdFormSupport extends FormSupport<Long> {

    Long getId();
}
