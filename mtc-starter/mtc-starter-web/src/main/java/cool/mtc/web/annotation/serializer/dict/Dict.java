package cool.mtc.web.annotation.serializer.dict;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cool.mtc.core.util.StringUtil;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 明河
 */
@JacksonAnnotationsInside
@JsonSerialize(using = DictSerializer.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dict {

    /**
     * 字典的类型
     */
    String type() default StringUtil.EMPTY;

    /**
     * 翻译的字典值的字段名称
     */
    String valueFieldName() default StringUtil.EMPTY;
}
