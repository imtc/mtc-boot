package cool.mtc.web.component.dict;

/**
 * @author yz
 * <p>
 * 用于定义枚举字典在页面上显示的样式颜色
 */
public interface EnumDictStyleColorSupport {

    String getStyleColor();
}
