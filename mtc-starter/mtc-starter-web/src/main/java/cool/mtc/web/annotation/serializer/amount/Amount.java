package cool.mtc.web.annotation.serializer.amount;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author yz
 */
@JacksonAnnotationsInside
@JsonSerialize(using = AmountSerializer.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Amount {

    /**
     * 小数位数
     */
    int decimalPlaces() default 2;
}
