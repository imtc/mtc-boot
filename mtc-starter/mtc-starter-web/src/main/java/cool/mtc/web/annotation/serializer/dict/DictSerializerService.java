package cool.mtc.web.annotation.serializer.dict;

/**
 * @author 明河
 */
public interface DictSerializerService {

    Object getValueByTypeAndKey(String type, Object key);
}
