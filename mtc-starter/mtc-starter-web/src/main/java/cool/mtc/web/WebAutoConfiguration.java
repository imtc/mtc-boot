package cool.mtc.web;

import cool.mtc.web.annotation.serializer.dict.DefaultDictSerializerServiceImpl;
import cool.mtc.web.annotation.serializer.dict.DictSerializerService;
import cool.mtc.web.config.WebConfigurer;
import cool.mtc.web.page.PageHandler;
import cool.mtc.web.result.GlobalResultWrapper;
import cool.mtc.web.result.ResultHandler;
import cool.mtc.web.util.SpringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author 明河
 */
@Configuration
@EnableConfigurationProperties({WebProperties.class})
@Import({
        WebConfigurer.class,
        GlobalResultWrapper.class,
        SpringUtil.class,
})
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WebAutoConfiguration {
    private final WebProperties webProperties;

    @Bean
    public PageHandler pageHandler() {
        return new PageHandler(webProperties);
    }

    @Bean
    public ResultHandler resultHandler() {
        return new ResultHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public DictSerializerService dictService() {
        return new DefaultDictSerializerServiceImpl();
    }
}
