package cool.mtc.web.component.dict;

import cool.mtc.core.enums.EnumSupport;

/**
 * @author yz
 * <p>
 * 用于枚举字典的翻译支持
 * 例如
 */
public interface EnumDictSupport extends EnumSupport {

    default String getDisplayName() {
        return getDesc();
    }
}
