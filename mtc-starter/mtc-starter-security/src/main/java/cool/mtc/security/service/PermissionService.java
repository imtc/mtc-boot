package cool.mtc.security.service;

/**
 * @author 明河
 */
public interface PermissionService {

    boolean hasRole(String... roleCodes);

    boolean hasPermission(String permission);
}
