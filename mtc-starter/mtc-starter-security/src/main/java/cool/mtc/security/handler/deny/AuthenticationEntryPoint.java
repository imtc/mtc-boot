package cool.mtc.security.handler.deny;

import cool.mtc.core.result.ResultConstant;
import cool.mtc.security.handler.HandleSupport;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 明河
 * <p>
 * 匿名用户访问无权限资源时的异常
 */
public class AuthenticationEntryPoint implements HandleSupport, org.springframework.security.web.AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex) {
        this.response(ResultConstant.A0301);
    }
}
