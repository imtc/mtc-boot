package cool.mtc.security.event;

import cool.mtc.security.auth.AuthFormSupport;
import cool.mtc.security.auth.AuthToken;
import lombok.Getter;
import lombok.ToString;

/**
 * @author yz
 */
@Getter
@ToString
public class AuthSuccessEvent extends AuthResultEvent {

    private final AuthToken authToken;

    public AuthSuccessEvent(Object source, String ip, String userAgent, AuthFormSupport authForm, AuthToken authToken) {
        super(source, ip, userAgent, authForm);
        this.authToken = authToken;
    }
}
