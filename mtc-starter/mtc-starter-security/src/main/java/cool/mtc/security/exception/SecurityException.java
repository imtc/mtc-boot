package cool.mtc.security.exception;

import cool.mtc.core.exception.CustomException;

/**
 * @author yz
 */
public class SecurityException extends CustomException {

    public SecurityException(String message) {
        super(message);
    }
}
