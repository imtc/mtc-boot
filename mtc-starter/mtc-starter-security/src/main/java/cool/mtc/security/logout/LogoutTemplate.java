package cool.mtc.security.logout;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yz
 */
@Component
@ConditionalOnMissingBean
public class LogoutTemplate {

    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
    }
}
