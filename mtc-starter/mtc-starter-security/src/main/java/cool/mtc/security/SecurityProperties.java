package cool.mtc.security;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 明河
 */
@Data
@ConfigurationProperties(prefix = "mtc.security")
public class SecurityProperties {

    /**
     * 是否启用
     * 默认启用
     */
    private boolean enabled = true;

    /**
     * 是否启用跨域
     * 默认不启用
     */
    private boolean cors = false;

    /**
     * 需要认证的Ant Patterns
     */
    private String[] authAntPatterns = {"/**"};

    /**
     * 忽略认证的Ant Patterns
     */
    private String[] ignoreAuthAntPatterns = {};

    /**
     * 使用JWT认证
     */
    private Auth authJwt = new Auth(true, "/api/**");

    /**
     * 使用密码认证
     */
    private String passwordAuthAntPattern = "/api/login/password";
    private Auth authPassword = new Auth(true, "/api/login/password");

    /**
     * 使用自定义认证
     */
    private Auth authCustom = new Auth();

    /**
     * 登出地址
     */
    private String logoutUrl = "/api/logout";

    @Data
    @NoArgsConstructor
    public static class Auth {

        /**
         * 是否启用
         */
        private boolean enabled = false;

        /**
         * 匹配路径的Ant Pattern
         */
        private String antPattern;

        public Auth(boolean enabled, String antPattern) {
            this.enabled = enabled;
            this.antPattern = antPattern;
        }
    }
}
