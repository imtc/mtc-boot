package cool.mtc.security.handler.logout;

import cool.mtc.security.handler.HandleSupport;
import cool.mtc.security.logout.LogoutTemplate;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 明河
 * <p>
 * 登出时的处理
 */
@Component
@Setter(onMethod = @__(@Autowired))
public class LogoutHandler implements HandleSupport, org.springframework.security.web.authentication.logout.LogoutHandler {
    private LogoutTemplate logoutTemplate;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        logoutTemplate.logout(request, response, authentication);
    }
}
