package cool.mtc.security.plugin.jwt;

import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

/**
 * @author 明河
 */
@Data
@ConfigurationProperties(prefix = "mtc.security.plugin.jwt")
public class JwtProperties {

    /**
     * 加密秘钥
     */
    private String key = "DEFAULT_KEY";

    /**
     * 签名算法
     */
    private SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

    /**
     * token有效时长
     * 在此时间范围内，token是有效的
     */
    private Duration validDuration = Duration.ofMinutes(30L);

    /**
     * token最终有效时长
     * 如果超出 validDuration 且在 finalValidDuration 范围内，认为是有效但过期的token，可以凭借此token获取新的有效token
     * 如果token超出此时间范围，认为是失效token
     */
    private Duration finalValidDuration = Duration.ofHours(6L);

}
