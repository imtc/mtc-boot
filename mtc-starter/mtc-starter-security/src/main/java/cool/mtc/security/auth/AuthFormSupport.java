package cool.mtc.security.auth;

import cool.mtc.core.result.ResultConstant;
import cool.mtc.core.util.JacksonUtil;
import cool.mtc.core.util.StringUtil;
import cool.mtc.security.exception.AuthException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author yz
 */
public interface AuthFormSupport {

    /**
     * 认证时的用户名
     * 记录登录日志的时候可能会用到
     * 如登录名、邮箱、手机号等，
     */
    default String getUsername() {
        return StringUtil.EMPTY;
    }

    static <T extends AuthFormSupport> T transForm(HttpServletRequest request, Class<T> clazz) {
        try {
            return JacksonUtil.OBJECT_MAPPER.readValue(request.getInputStream(), clazz);
        } catch (IOException ex) {
            throw new AuthException(ResultConstant.A0400, "登录参数错误");
        }
    }
}
