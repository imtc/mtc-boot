package cool.mtc.security.auth;

import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author yz
 */
@Getter
public abstract class AuthToken extends AbstractAuthenticationToken {

    private final String authWay;
    private final String token;

    public AuthToken(Collection<? extends GrantedAuthority> authorities, String authWay, String token) {
        super(authorities);
        this.authWay = authWay;
        this.token = token;
    }

    public abstract AuthFormSupport getForm();

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
