package cool.mtc.security.auth.custom;

import cool.mtc.security.auth.AuthTemplate;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * @author 明河
 */
@Setter(onMethod = @__(@Autowired))
public class CustomAuthProvider implements AuthenticationProvider {
    private AuthTemplate authTemplate;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return authTemplate.authenticate(authentication);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return CustomAuthToken.class.isAssignableFrom(aClass);
    }
}
