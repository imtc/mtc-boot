package cool.mtc.security.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * @author yz
 */
public interface CustomSecurityFilterChain {

    void configure(HttpSecurity http) throws Exception;
}
