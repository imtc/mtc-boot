package cool.mtc.security.auth.password;

import cool.mtc.security.auth.AuthToken;
import cool.mtc.security.constant.AuthConstant;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author 明河
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class PasswordAuthToken extends AuthToken {
    private final PasswordAuthForm form;

    public PasswordAuthToken(PasswordAuthForm form) {
        super(null, null, null);
        this.form = form;
        super.setAuthenticated(false);
    }

    public PasswordAuthToken(PasswordAuthForm form, String token, Collection<? extends GrantedAuthority> authorities) {
        super(authorities, AuthConstant.AUTH_WAY_PASSWORD, token);
        this.form = form;
        super.setAuthenticated(true);
    }
}
