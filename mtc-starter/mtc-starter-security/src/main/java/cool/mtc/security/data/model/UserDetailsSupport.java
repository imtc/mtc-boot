package cool.mtc.security.data.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author yz
 */
public interface UserDetailsSupport extends UserDetails {

    Object getUserId();

    Object getOrgId();

    default List<String> getRoleList() {
        return new ArrayList<>();
    }

    default List<String> getPermissionList() {
        return new ArrayList<>();
    }

    /**
     * 检查账号状态
     */
    void handleCheckUserStatus();

    @Override
    default Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @Override
    default boolean isAccountNonExpired() {
        return true;
    }

    @Override
    default boolean isAccountNonLocked() {
        return true;
    }

    @Override
    default boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    default boolean isEnabled() {
        return true;
    }
}
