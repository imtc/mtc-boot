package cool.mtc.security.auth.jwt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cool.mtc.security.auth.AuthFormSupport;
import cool.mtc.security.util.SecurityUtil;
import io.jsonwebtoken.Claims;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yz
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class JwtAuthForm implements AuthFormSupport {

    private String token;

    private boolean expired = false;

    private Claims tokenCalims;

    public static JwtAuthForm transForm(HttpServletRequest request) {
        String token = SecurityUtil.getTokenFromRequest(request);
        JwtAuthForm form = new JwtAuthForm();
        form.setToken(token);
        return form;
    }
}
