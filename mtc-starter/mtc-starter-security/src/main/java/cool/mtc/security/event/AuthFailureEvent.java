package cool.mtc.security.event;

import cool.mtc.security.auth.AuthFormSupport;
import lombok.Getter;
import lombok.ToString;

/**
 * @author yz
 */
@Getter
@ToString
public class AuthFailureEvent extends AuthResultEvent {

    private final String authWay;

    /**
     * 认证失败的原因描述
     */
    private final String description;

    public AuthFailureEvent(Object source, String ip, String userAgent, AuthFormSupport authForm, String authWay, String description) {
        super(source, ip, userAgent, authForm);
        this.authWay = authWay;
        this.description = description;
    }
}
