package cool.mtc.security.event;

import cool.mtc.security.auth.AuthFormSupport;
import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/**
 * @author yz
 */
@Getter
@ToString
public class AuthResultEvent extends ApplicationEvent {

    private final String ip;
    private final String userAgent;

    /**
     * 认证的信息
     */
    private final AuthFormSupport authForm;

    public AuthResultEvent(Object source, String ip, String userAgent, AuthFormSupport authForm) {
        super(source);
        this.ip = ip;
        this.userAgent = userAgent;
        this.authForm = authForm;
    }
}
