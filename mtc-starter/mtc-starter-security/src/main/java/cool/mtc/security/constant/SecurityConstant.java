package cool.mtc.security.constant;

/**
 * @author yz
 */
public class SecurityConstant {

    /**
     * token在请求头中的key
     */
    public static final String PARAM_TOKEN_KEY = "Authorization";

    /**
     * token在请求头中值的前缀
     */
    public static final String PARAM_TOKEN_PREFIX = "Bearer ";

    /**
     * 请求对象中存储请求认证表单的key
     */
    public static final String REQUEST_ATTRIBUTE_KEY_AUTH_FORM = "AuthFrom";

    public static final String REQUEST_ATTRIBUTE_KEY_AUTH_WAY = "AuthWay";
}
