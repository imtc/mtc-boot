package cool.mtc.security.auth.jwt;

import cool.mtc.security.auth.AuthToken;
import cool.mtc.security.constant.AuthConstant;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author 明河
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class JwtAuthToken extends AuthToken {
    private final JwtAuthForm form;

    public JwtAuthToken(JwtAuthForm form) {
        super(null, null, null);
        this.form = form;
        super.setAuthenticated(false);
    }

    public JwtAuthToken(JwtAuthForm form, String token, Collection<? extends GrantedAuthority> authorities) {
        super(authorities, AuthConstant.AUTH_WAY_JWT, token);
        this.form = form;
        super.setAuthenticated(true);
    }
}
