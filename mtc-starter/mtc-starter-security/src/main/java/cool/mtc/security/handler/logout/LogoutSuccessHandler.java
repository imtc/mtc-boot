package cool.mtc.security.handler.logout;

import cool.mtc.core.result.ResultConstant;
import cool.mtc.security.handler.HandleSupport;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 明河
 * <p>
 * 登出成功后的处理
 */
@Component
public class LogoutSuccessHandler implements HandleSupport, org.springframework.security.web.authentication.logout.LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        this.response(ResultConstant.OK);
    }
}
