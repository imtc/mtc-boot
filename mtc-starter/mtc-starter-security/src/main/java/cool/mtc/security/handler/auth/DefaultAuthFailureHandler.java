package cool.mtc.security.handler.auth;

import cool.mtc.core.util.StringUtil;
import cool.mtc.security.auth.AuthFormSupport;
import cool.mtc.security.constant.SecurityConstant;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 明河
 */
@Component
public class DefaultAuthFailureHandler extends AuthFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex) {
        this.fail(ex);

        Object authWay = request.getAttribute(SecurityConstant.REQUEST_ATTRIBUTE_KEY_AUTH_WAY);
        AuthFormSupport form = (AuthFormSupport) request.getAttribute(SecurityConstant.REQUEST_ATTRIBUTE_KEY_AUTH_FORM);
        super.publishAuthFailureEvent(request, form, StringUtil.ifNull(authWay), ex.getMessage());
    }
}
