package cool.mtc.security.exception;

import cool.mtc.core.result.Result;
import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

/**
 * @author 明河
 */
@Getter
public class AuthException extends AuthenticationException {
    private final Result<Object> result;

    public AuthException(Result<Object> result) {
        this(result, result.getMsg());
    }

    public AuthException(Result<Object> result, String msg) {
        super(msg);
        this.result = result.newInstance().msg(msg);
    }
}
