package cool.mtc.security.plugin.jwt;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yz
 */
@Configuration
@EnableConfigurationProperties(JwtProperties.class)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class JwtConfig {
    private final JwtProperties jwtProperties;

    @Bean
    public JwtTemplate jwtTemplate() {
        return new JwtTemplate(jwtProperties);
    }
}
