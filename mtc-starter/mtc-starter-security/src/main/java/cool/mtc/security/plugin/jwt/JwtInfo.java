package cool.mtc.security.plugin.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yz
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtInfo {
    private String id;
    private String token;
}
