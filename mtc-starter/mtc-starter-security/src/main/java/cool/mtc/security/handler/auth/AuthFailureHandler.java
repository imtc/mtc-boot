package cool.mtc.security.handler.auth;

import cool.mtc.core.result.ResultConstant;
import cool.mtc.security.exception.AuthException;
import cool.mtc.security.handler.HandleSupport;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * @author 明河
 */
public abstract class AuthFailureHandler extends AuthHandler implements HandleSupport, AuthenticationFailureHandler {

    public void fail(AuthenticationException ex) {
        AuthException authException = ex instanceof AuthException ? (AuthException) ex : new AuthException(ResultConstant.A0300);
        this.response(authException.getResult());
    }
}
