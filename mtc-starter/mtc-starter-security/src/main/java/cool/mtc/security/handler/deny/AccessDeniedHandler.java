package cool.mtc.security.handler.deny;

import cool.mtc.core.result.ResultConstant;
import cool.mtc.security.handler.HandleSupport;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 明河
 * <p>
 * 认证用户访问无权限资源时的异常
 */
public class AccessDeniedHandler implements HandleSupport, org.springframework.security.web.access.AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex) {
        this.response(ResultConstant.A0301);
    }
}
