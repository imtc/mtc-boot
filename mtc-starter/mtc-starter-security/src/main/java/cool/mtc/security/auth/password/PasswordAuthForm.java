package cool.mtc.security.auth.password;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cool.mtc.security.auth.AuthFormSupport;
import lombok.Data;

/**
 * @author 明河
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordAuthForm implements AuthFormSupport {

    private String username;
    private String password;

    /**
     * 验证码
     */
    public Object verifyCode;
}
