package cool.mtc.security.constant;

/**
 * @author yz
 */
public class AuthConstant {

    // 认证方式
    public static final String AUTH_WAY_PASSWORD = "PASSWORD";
    public static final String AUTH_WAY_JWT = "JWT";
    public static final String AUTH_WAY_CUSTOM = "CUSTOM";
}
