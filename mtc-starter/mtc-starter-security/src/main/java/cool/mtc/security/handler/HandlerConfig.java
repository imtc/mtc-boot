package cool.mtc.security.handler;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author yz
 */
@Configuration
@ComponentScan
public class HandlerConfig {
}
