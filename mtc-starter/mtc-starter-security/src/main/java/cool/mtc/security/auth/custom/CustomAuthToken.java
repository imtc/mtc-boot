package cool.mtc.security.auth.custom;

import cool.mtc.security.auth.AuthToken;
import cool.mtc.security.constant.AuthConstant;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author 明河
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class CustomAuthToken extends AuthToken {
    private final CustomAuthFormSupport form;

    @Setter
    private String token;

    public CustomAuthToken(CustomAuthFormSupport form) {
        super(null, null, null);
        this.form = form;
        super.setAuthenticated(false);
    }

    public CustomAuthToken(CustomAuthFormSupport form, String token, Collection<? extends GrantedAuthority> authorities) {
        super(authorities, AuthConstant.AUTH_WAY_CUSTOM, token);
        this.form = form;
        super.setAuthenticated(true);
    }
}
