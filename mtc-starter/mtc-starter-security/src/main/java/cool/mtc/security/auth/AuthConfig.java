package cool.mtc.security.auth;

import cool.mtc.security.SecurityProperties;
import cool.mtc.security.auth.custom.CustomAuthTemplate;
import cool.mtc.security.auth.jwt.JwtAuthTemplate;
import cool.mtc.security.auth.password.PasswordAuthTemplate;
import cool.mtc.security.exception.SecurityException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author yz
 */
@Configuration
@ComponentScan
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthConfig {
    private final SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean
    public CustomAuthTemplate customAuthTemplate() {
        if (securityProperties.getAuthCustom().isEnabled()) {
            throw new SecurityException(String.format("未定义[%s]类型的Bean", CustomAuthTemplate.class));
        }
        return null;
    }

    @Bean
    @ConditionalOnMissingBean
    public JwtAuthTemplate jwtAuthTemplate() {
        if (securityProperties.getAuthJwt().isEnabled()) {
            throw new SecurityException(String.format("未定义[%s]类型的Bean", JwtAuthTemplate.class));
        }
        return null;
    }

    @Bean
    @ConditionalOnMissingBean
    public PasswordAuthTemplate passwordAuthTemplate() {
        if (securityProperties.getAuthPassword().isEnabled()) {
            throw new SecurityException(String.format("未定义[%s]类型的Bean", PasswordAuthTemplate.class));
        }
        return null;
    }
}
