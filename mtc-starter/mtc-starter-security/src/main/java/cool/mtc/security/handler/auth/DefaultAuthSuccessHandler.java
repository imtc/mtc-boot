package cool.mtc.security.handler.auth;

import cool.mtc.security.auth.AuthToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yz
 */
@Component
public class DefaultAuthSuccessHandler extends AuthSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        this.success(authentication);
        AuthToken authToken = (AuthToken) authentication;
        super.publishAuthSuccessEvent(request, authToken.getForm(), authToken);
    }
}
