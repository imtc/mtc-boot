package cool.mtc.security.util;


import cool.mtc.core.util.StringUtil;
import cool.mtc.security.constant.SecurityConstant;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 明河
 */
public abstract class SecurityUtil {

    /**
     * 从请求中获取token
     */
    public static String getTokenFromRequest(HttpServletRequest request) {
        String token = request.getHeader(SecurityConstant.PARAM_TOKEN_KEY);
        if (StringUtil.isEmpty(token) || !token.startsWith(SecurityConstant.PARAM_TOKEN_PREFIX)) {
            return null;
        }
        return token.replaceFirst(SecurityConstant.PARAM_TOKEN_PREFIX, StringUtil.EMPTY);
    }
}
