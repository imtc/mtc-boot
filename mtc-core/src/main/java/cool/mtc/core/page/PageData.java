package cool.mtc.core.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 明河
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class PageData<T> {
    private List<T> data = new ArrayList<>();
    private long total = 0L;
}
