package cool.mtc.core.enums;


import cool.mtc.core.util.StringUtil;

/**
 * @author 明河
 */
public interface EnumSupport {

    String name();

    String getDesc();

    static String getDesc(EnumSupport enumSupport) {
        return null == enumSupport ? null : enumSupport.getDesc();
    }

    static <T extends Enum<T>> T parse(Class<T> enumType, String name) {
        if (StringUtil.isEmpty(name)) {
            return null;
        }
        return T.valueOf(enumType, name);
    }
}
