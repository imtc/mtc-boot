package cool.mtc.core.exception;

/**
 * @author 明河
 */
public class StartupException extends Exception {

    public StartupException(String message) {
        super(message);
    }
}
