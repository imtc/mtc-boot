package cool.mtc.core.exception;

import cool.mtc.core.result.Result;
import cool.mtc.core.result.ResultConstant;
import lombok.Getter;

/**
 * @author 明河
 */
@Getter
public class CustomException extends RuntimeException {
    private final Result<Object> result;

    public CustomException(String message) {
        this(ResultConstant.ERROR.msg(message));
    }

    public CustomException(Result<Object> result) {
        super(result.getMsg());
        this.result = result;
    }

    public CustomException(String message, Throwable throwable) {
        this(ResultConstant.ERROR.msg(message), throwable);
    }

    public CustomException(Result<Object> result, Throwable throwable) {
        super(result.getMsg(), throwable);
        this.result = result;
    }
}
