package cool.mtc.core.exception;

import cool.mtc.core.result.Result;

/**
 * @author 明河
 */
public class ControllerException extends CustomException {

    public ControllerException(String message) {
        super(message);
    }

    public ControllerException(Result<Object> result) {
        super(result);
    }

    public ControllerException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ControllerException(Result<Object> result, Throwable throwable) {
        super(result, throwable);
    }
}
