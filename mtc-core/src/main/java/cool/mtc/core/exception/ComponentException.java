package cool.mtc.core.exception;

import cool.mtc.core.result.Result;

/**
 * @author yz
 */
public class ComponentException extends CustomException{

    public ComponentException(String message) {
        super(message);
    }

    public ComponentException(Result<Object> result) {
        super(result);
    }

    public ComponentException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ComponentException(Result<Object> result, Throwable throwable) {
        super(result, throwable);
    }
}
