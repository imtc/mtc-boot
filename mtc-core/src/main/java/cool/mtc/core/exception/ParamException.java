package cool.mtc.core.exception;

/**
 * @author yz
 */
public class ParamException extends CustomException {

    public ParamException(String message) {
        super(message);
    }
}
