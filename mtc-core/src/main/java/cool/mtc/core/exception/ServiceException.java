package cool.mtc.core.exception;

import cool.mtc.core.result.Result;

/**
 * @author 明河
 */
public class ServiceException extends CustomException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Result<Object> result) {
        super(result);
    }

    public ServiceException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ServiceException(Result<Object> result, Throwable throwable) {
        super(result, throwable);
    }
}
