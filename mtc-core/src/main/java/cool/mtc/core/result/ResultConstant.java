package cool.mtc.core.result;

/**
 * @author 明河
 */
public abstract class ResultConstant {

    public static final Result<Object> OK = Result.of(0, "成功");
    public static final Result<Object> ERROR = Result.of(-1, "错误");

    // region 阿里巴巴Java开发手册错误码列表
    // region Axx
    // A01 用户注册错误
    public static final Result<Object> A0100 = Result.of(110100, "用户注册错误");
    public static final Result<Object> A0110 = Result.of(110110, "用户名校验失败");
    public static final Result<Object> A0120 = Result.of(110120, "密码校验失败");
    public static final Result<Object> A0130 = Result.of(110130, "校验码输入错误");
    public static final Result<Object> A0131 = Result.of(110130, "短信校验码输入错误");
    public static final Result<Object> A0132 = Result.of(110130, "邮件校验码输入错误");
    public static final Result<Object> A0133 = Result.of(110130, "语音校验码输入错误");

    // A02 用户登录异常
    public static final Result<Object> A0200 = Result.of(110200, "用户登录异常");
    public static final Result<Object> A0201 = Result.of(110201, "用户账户不存在");
    public static final Result<Object> A0210 = Result.of(110210, "用户密码错误");
    public static final Result<Object> A0220 = Result.of(110210, "用户身份校验失败");
    public static final Result<Object> A0230 = Result.of(110210, "用户登录已过期");
    public static final Result<Object> A0240 = Result.of(110210, "用户验证码错误");
    public static final Result<Object> A0241 = Result.of(110210, "用户验证码尝试次数超限");
    // A03 访问权限异常
    public static final Result<Object> A0300 = Result.of(110300, "访问权限异常");
    public static final Result<Object> A0301 = Result.of(110301, "访问未授权");
    public static final Result<Object> A0311 = Result.of(110311, "授权已过期");
    // A04 用户请求参数错误
    public static final Result<Object> A0400 = Result.of(110400, "参数异常");
    public static final Result<Object> A0401 = Result.of(110401, "包含非法恶意跳转链接");
    public static final Result<Object> A0402 = Result.of(110402, "无效的用户输入");
    public static final Result<Object> A0410 = Result.of(110410, "请求必填参数为空");
    // A05 用户请求服务异常
    public static final Result<Object> A0500 = Result.of(110500, "用户请求服务异常");
    public static final Result<Object> A0501 = Result.of(110501, "请求次数超出限制");
    public static final Result<Object> A0502 = Result.of(110502, "请求并发数超出限制");
    public static final Result<Object> A0503 = Result.of(110503, "用户操作请等待");
    public static final Result<Object> A0504 = Result.of(110504, "WebSocket连接异常");
    public static final Result<Object> A0505 = Result.of(110505, "WebSocket连接断开");
    public static final Result<Object> A0506 = Result.of(110506, "用户重复请求");
    // endregion
    // region Cxx
    public static final Result<Object> C0001 = Result.of(130001, "调用第三方服务出错");
    public static final Result<Object> C0100 = Result.of(130100, "中间件服务出错");
    public static final Result<Object> C0200 = Result.of(130200, "第三方系统执行超时");
    public static final Result<Object> C0300 = Result.of(130300, "数据库服务出错");
    public static final Result<Object> C0400 = Result.of(130400, "第三方容灾系统被触发");
    public static final Result<Object> C0500 = Result.of(130500, "通知服务出错");
    public static final Result<Object> C0501 = Result.of(130501, "短信提醒服务失败");
    public static final Result<Object> C0502 = Result.of(130502, "语音提醒服务失败");
    public static final Result<Object> C0503 = Result.of(130503, "邮件提醒服务失败");
    // endregion
    // endregion
}
