package cool.mtc.core.util;

import java.util.*;

/**
 * @author 明河
 */
public abstract class CollectionUtil {

    public static boolean isEmpty(Collection<?> collection) {
        return null == collection || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    public static <T> List<T> listMoreThan(List<T> aList, List<T> bList) {
        List<T> list = new ArrayList<>();
        for (T item : aList) {
            if (bList.stream().noneMatch(e -> e.equals(item))) {
                list.add(item);
            }
        }
        return list;
    }

    public static <T> List<T> transList(T... t) {
        switch (t.length) {
            case 0:
                return new ArrayList<>();
            case 1:
                return new ArrayList<>(Collections.singletonList(t[0]));
            default:
                return new ArrayList<>(Arrays.asList(t));
        }
    }
}
