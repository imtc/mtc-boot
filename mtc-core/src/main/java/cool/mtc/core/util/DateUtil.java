package cool.mtc.core.util;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author 明河
 */
public abstract class DateUtil {

    /**
     * LocalDateTime 转 Date
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Date 转 LocalDateTime
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }


    /**
     * 指定日期当天的开始时间
     */
    public static LocalDateTime startOfDay(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        }
        return LocalDateTime.of(localDateTime.toLocalDate(), LocalTime.MIN);
    }

    /**
     * 指定日期当天的开始时间
     */
    public static Date startOfDay(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endLocalDateTime = startOfDay(localDateTime);
        return localDateTimeToDate(endLocalDateTime);
    }


    /**
     * 指定日期当天的结束时间
     */
    public static LocalDateTime endOfDay(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        }
        return LocalDateTime.of(localDateTime.toLocalDate(), LocalTime.MAX);
    }

    /**
     * 指定日期当天的结束时间
     */
    public static Date endOfDay(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endLocalDateTime = endOfDay(localDateTime);
        return localDateTimeToDate(endLocalDateTime);
    }
}
